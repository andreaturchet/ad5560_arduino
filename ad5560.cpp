#include "Arduino.h"
#include "ad5560.h"


//********************************************************************************************************

bool ad5560::writeRegister( uint16_t data_to_write, uint8_t address)
{
  uint16_t _data_to_write = data_to_write;
  return writeRegisterCore( _data_to_write, address, 1);
}

bool ad5560::readRegister( uint16_t &data_from_slave, uint8_t address)
{
  uint16_t noData = 0;
  if( writeRegisterCore( noData, address, 0)) //request read register
  {
    return writeRegisterCore( data_from_slave, NOP_REG, 1); //get readed register
  }
  return 0;
}

void ad5560::dacCellCompleteSetup( uint16_t firstDacAddress, uint16_t x1_dac, uint16_t m_dac, uint16_t c_dac)
{
  writeRegister( x1_dac, firstDacAddress);
  writeRegister( m_dac, firstDacAddress+1);
  writeRegister( c_dac, firstDacAddress+2);
}

bool ad5560::registerDownloadAndCheck()
{
	bool done = 1;
	uint16_t bufferReg = 0;
	//*******
	done &= readRegister( bufferReg, SYS_CRTL_REG);
	if( bufferReg != configRegister.sysCtrlReg)
	{
		done &= 0;
		writeRegister( configRegister.sysCtrlReg, SYS_CRTL_REG);
	}
	//********
	done &= readRegister( bufferReg, DPS_REG_1);
	if( bufferReg != configRegister.dpsReg1)
	{
		done &= 0;
		writeRegister( configRegister.dpsReg1, DPS_REG_1);
	}
	//********
	done &= readRegister( bufferReg, DPS_REG_2);
	if( bufferReg != configRegister.dpsReg2)
	{
		done &= 0;
		writeRegister( configRegister.dpsReg2, DPS_REG_2);
	}
	//********
	done &= readRegister( bufferReg, COMP_REG_1);
	if( bufferReg != configRegister.compReg1)
	{
		done &= 0;
		writeRegister( configRegister.compReg1, COMP_REG_1);
	}
	//********
	done &= readRegister( bufferReg, COMP_REG_2);
	if( bufferReg != configRegister.compReg2)
	{
		done &= 0;
		writeRegister( configRegister.compReg2, COMP_REG_2);
	}
	//********
	done &= readRegister( bufferReg, ALARM_SET_REG);
	if( bufferReg != configRegister.alarmSetReg)
	{
		done &= 0;
		writeRegister( configRegister.alarmSetReg, ALARM_SET_REG);
	}
	return done;
}
//**************************************************************************************************

bool ad5560::startUp( uint16_t busyPin, uint16_t syncPin, uint16_t analogPin, uint32_t spiClk)
{	
	analog_pin = analogPin;
	busy_pin = busyPin;
	sync_pin = syncPin;
	spi_clk = spiClk;
	setSupplyRail( 15, -15);
  SPI.begin();
  pinMode( sync_pin, OUTPUT);
  digitalWrite( sync_pin, SYNC_LOW);
  pinMode( busy_pin, INPUT);
  pinMode( CLK_COUNT_PIN, INPUT);
  pinMode( RCLK, OUTPUT);
  //------------------------
  TCCR4A = CLC_TIMER_4_STOP; // no toogle
  TCCR4B = 0b00001011; // clear time on compare, 64 prescaler
  TCCR4C = 0b10000000;
  OCR4A = 250; // (16M/64)/F   F=1000
  
  //counter 5 counts whether the ramp is finished
  TCCR5A = 0b00000000;
  TCCR5B = CLC_EXT_RISE_B_DIS; //0b00001111
  TCCR5C = 0b00000000;
  OCR5A = 0;
  TIMSK5 |= (1 << OCIE1A);
	
	//-------------------------
	bool done = 1;
  configRegister.sysCtrlReg = SYS_CTRL_REG_DEF0;
	configRegister.dpsReg1 = DPS_REG_1_DEF0;
	configRegister.dpsReg2 = DPS_REG_2_DEF0;
	configRegister.compReg1 = COMP_REG_1_DEF0;
	configRegister.compReg2 = COMP_REG_2_DEF0;
	done &= writeRegister( configRegister.sysCtrlReg, SYS_CRTL_REG);
  done &= writeRegister( configRegister.dpsReg1, DPS_REG_1);
  done &= writeRegister( configRegister.dpsReg2, DPS_REG_2);
  done &= writeRegister( configRegister.compReg1, COMP_REG_1);
  done &= writeRegister( configRegister.compReg2, COMP_REG_2);
	return done;
}

void ad5560::setSupplyRail( double positiveRail, double negativeRail)
{
	positiveRailVoltage = positiveRail;
	negativeRailVoltage = negativeRail;
	setOffsetAutomatic();
}

bool ad5560::setOffsetAutomatic()
{
	double offsetVoltage = 0.5 - ( positiveRailVoltage + negativeRailVoltage) / ( 2 * vRef * REF_AMP_GAIN);
	offsetVoltage *= 0xffff;
	offsetDacCode = offsetVoltage;
	return writeRegister( configRegister.sysCtrlReg, OFFSET_DAC_REG);
}

void ad5560::setVRef( double V_Ref)
{
	vRef = V_Ref;
}

double ad5560::getVRef()
{
	return vRef;
}

//----------------------------------------------------------SYS_CRTL_REG
bool ad5560::setShutdownTemp( uint8_t temperature)
{
	if( temperature < 4)
	{
		configRegister.sysCtrlReg = bitEditor( 14, 2, configRegister.sysCtrlReg, temperature);
		return writeRegister( configRegister.sysCtrlReg, SYS_CRTL_REG);
	}
	return 0;
}

bool ad5560::setMeasureOutputGain( uint8_t gain)
{
	if( gain < 4)
	{
		configRegister.sysCtrlReg = bitEditor( 12, 2, configRegister.sysCtrlReg, gain);
		return writeRegister( configRegister.sysCtrlReg, SYS_CRTL_REG);
	}
	return 0;
}

uint8_t ad5560::getMeasureOutputGain()
{
	return bitDecoder( 12, 2, configRegister.sysCtrlReg);
}

bool ad5560::setFinGnd( bool state)
{
	if( state)
		configRegister.sysCtrlReg |= 0b0000100000000000;
	else
		configRegister.sysCtrlReg &= 0b1111011111111111;
	return writeRegister( configRegister.sysCtrlReg, SYS_CRTL_REG);
}
bool ad5560::getFinGnd()
{
	return (configRegister.sysCtrlReg & 0b0000100000000000) > 0;
}

bool ad5560::setCpo( bool state)
{
	if( state)
		configRegister.sysCtrlReg |= 0b0000010000000000;
	else
		configRegister.sysCtrlReg &= 0b1111101111111111;
	return writeRegister( configRegister.sysCtrlReg, SYS_CRTL_REG);
}
bool ad5560::getCpo()
{
	return (configRegister.sysCtrlReg & 0b0000100000000000) > 0;
}

bool ad5560::setPd( bool state)
{
	if( state)
		configRegister.sysCtrlReg |= 0b0000001000000000;
	else
		configRegister.sysCtrlReg &= 0b1111110111111111;
	return writeRegister( configRegister.sysCtrlReg, SYS_CRTL_REG);
}
bool ad5560::getPd()
{
	return (configRegister.sysCtrlReg & 0b0000001000000000) > 0;
}


bool ad5560::setLoadMode( uint8_t mode)
{
	if( mode < 4)
	{
		configRegister.sysCtrlReg = bitEditor( 7, 2, configRegister.sysCtrlReg, mode);
		return writeRegister( configRegister.sysCtrlReg, SYS_CRTL_REG);
	}
	return 0;
}
uint8_t ad5560::getLoadMode()
{
	return bitDecoder( 7, 2, configRegister.sysCtrlReg);
}
//----------------------------------------------------------DPS_REG_1
bool ad5560::forceEnable( bool enable)
{
	if( enable)
		configRegister.dpsReg1 |= 0b1000000000000000;
	else
		configRegister.dpsReg1 &= 0b0111111111111111;
	return writeRegister( configRegister.dpsReg1, DPS_REG_1);
}

bool ad5560::forceState()
{
	return (configRegister.dpsReg1 & 0b1000000000000000) > 0;
}

bool ad5560::setOutputCurrentRange( uint8_t currentRange)
{
	if( currentRange < 8)
	{
		configRegister.dpsReg1 = bitEditor( 11, 3, configRegister.dpsReg1, currentRange); //11 bit offset, 3 bit length field
		return writeRegister( configRegister.dpsReg1, DPS_REG_1);
	}
	return 0;
}

uint8_t ad5560::getOutputCurrent()
{
	return (uint8_t)(( configRegister.dpsReg1 >> 11) & 0x0007);
}

bool ad5560::setComparatorMode( uint8_t mode)
{
	if( mode < 4)
	{
		configRegister.dpsReg1 = bitEditor( 9, 2, configRegister.dpsReg1, mode); //9 bit offset, 2 bit length field	
		return writeRegister( configRegister.dpsReg1, DPS_REG_1);
	}
	return 0;
}

uint8_t ad5560::getComparatorMode()
{
	return (uint8_t)(( configRegister.dpsReg1 >> 9) & 0x0003);
}

bool ad5560::enableMeasureOutput( bool enable)
{
	if( enable)
	{
		configRegister.dpsReg1 |= 0b0000000100000000;
		return writeRegister( configRegister.dpsReg1, DPS_REG_1);
	}	
	configRegister.dpsReg1 &= 0b1111111011111111;	
	return writeRegister( configRegister.dpsReg1, DPS_REG_1);
}

bool ad5560::setMeasureOutput( uint8_t mode)
{
	if( mode < 7)
	{
		configRegister.dpsReg1 = bitEditor( 5, 3, configRegister.dpsReg1, mode); //9 bit offset, 2 bit length field	
		return writeRegister( configRegister.dpsReg1, DPS_REG_1);
	}
	return 0;
}

uint8_t ad5560::getMeasureOutput()
{
	return (uint8_t)(( configRegister.dpsReg1 >> 5) & 0x0007);
}

bool ad5560::enableClamp( bool enable)
{
	if( enable)
	{
		configRegister.dpsReg1 |= 0b0000000000010000;
		return writeRegister( configRegister.dpsReg1, DPS_REG_1);
	}	
	configRegister.dpsReg1 &= 0b1111111111101111;	
	return writeRegister( configRegister.dpsReg1, DPS_REG_1);
}

//----------------------------------------------------------------DPS_REG_2
bool ad5560::setSlewRate( uint8_t sr)
{
	if( sr < 8)
	{
		configRegister.dpsReg2 = bitEditor( 12, 3, configRegister.dpsReg2, sr); //9 bit offset, 2 bit length field	
		return writeRegister( configRegister.dpsReg2, DPS_REG_2);
	}
	return 0;
}

uint8_t ad5560::getSlewRate()
{
	return bitDecoder( 12, 3, configRegister.dpsReg2);
}

bool ad5560::setGpo( bool state)
{
	if( state)
	{
		configRegister.dpsReg2 |= 0b0000100000000000;
		return writeRegister( configRegister.dpsReg2, DPS_REG_2);
	}	
	configRegister.dpsReg2 &= 0b1111011111111111;	
	return writeRegister( configRegister.dpsReg2, DPS_REG_2);
}

bool ad5560::setSlaveMode( uint8_t mode)
{
	if( mode < 4)
	{
		configRegister.dpsReg2 = bitEditor( 9, 2, configRegister.dpsReg2, mode); //9 bit offset, 2 bit length field	
		return writeRegister( configRegister.dpsReg2, DPS_REG_2);
	}
	return 0;
}

uint8_t ad5560::getSlaveMode()
{
	return bitDecoder( 9, 2, configRegister.dpsReg2);
}

bool ad5560::setSenseForce( uint8_t mode)
{
	switch( mode)
	{
		default:
			return 0;
		break;
		case 0: //sfo = 0, guard = 0
			configRegister.dpsReg2 &= 0b0111111101111111;	
			return writeRegister( configRegister.dpsReg2, DPS_REG_2);
		break;
		case 1://sfo = 1, guard = 0
			configRegister.dpsReg2 &= 0b0111111101111111;	
			configRegister.dpsReg2 |= 0b1000000000000000;	
			return writeRegister( configRegister.dpsReg2, DPS_REG_2);
		break;
		case 2://sfo = 0, guard = 1
			configRegister.dpsReg2 &= 0b0111111101111111;	
			configRegister.dpsReg2 |= 0b0000000010000000;	
			return writeRegister( configRegister.dpsReg2, DPS_REG_2);
		break;
		case 3://sfo = 1, guard = 1
			configRegister.dpsReg2 |= 0b1000000010000000;		
			return writeRegister( configRegister.dpsReg2, DPS_REG_2);
		break;
	}
}

bool ad5560::setInt_10k( bool state)
{
	if( state)
	{
		configRegister.dpsReg2 |= 0b0000000100000000;
		return writeRegister( configRegister.dpsReg2, DPS_REG_2);
	}	
	configRegister.dpsReg2 &= 0b1111111011111111;	
	return writeRegister( configRegister.dpsReg2, DPS_REG_2);
}

//-------------------------------------------------------------COMP_REG_1
bool ad5560::setC_Dut( uint8_t cDut)
{
	if( cDut < 16)
	{
		configRegister.compReg1 = bitEditor( 12, 4, configRegister.compReg1, cDut); //9 bit offset, 2 bit length field	
		return writeRegister( configRegister.compReg1, COMP_REG_1);
	}
	return 0;
}

bool ad5560::setC_DutEsr( uint8_t esr)
{
	if( esr < 16)
	{
		configRegister.compReg1 = bitEditor( 8, 4, configRegister.compReg1, esr); //8 bit offset, 4 bit length field	
		return writeRegister( configRegister.compReg1, COMP_REG_1);
	}
	return 0;
}

bool ad5560::setSafeMode( bool state)
{
	if( state)
	{
		configRegister.compReg1 |= 0b0000000010000000;
		return writeRegister( configRegister.compReg1, COMP_REG_1);
	}	
	configRegister.compReg1 &= 0b1111111101111111;	
	return writeRegister( configRegister.compReg1, COMP_REG_1);
}

//--------------------------------------------------------------COMP_REG_2
bool ad5560::setManualCompensation( bool state)
{
	if( state)
	{
		configRegister.compReg2 |= 0b1000000000000000;
		return writeRegister( configRegister.compReg2, COMP_REG_2);
	}	
	configRegister.compReg2 &= 0b0111111111111111;	
	return writeRegister( configRegister.compReg2, COMP_REG_2);
}

bool ad5560::setRz( uint8_t rz)
{
	if( rz < 8)
	{
		configRegister.compReg2 = bitEditor( 12, 3, configRegister.compReg2, rz); //12 bit offset, 3 bit length field	
		return writeRegister( configRegister.compReg2, COMP_REG_2);
	}
	return 0;
}

bool ad5560::setRp( uint8_t rp)
{
	if( rp < 8)
	{
		configRegister.compReg2 = bitEditor( 9, 3, configRegister.compReg2, rp); //9 bit offset, 3 bit length field	
		return writeRegister( configRegister.compReg2, COMP_REG_2);
	}
	return 0;
}

bool ad5560::setGm( uint8_t gm)
{
	if( gm < 8)
	{
		configRegister.compReg2 = bitEditor( 7, 2, configRegister.compReg2, gm); //7 bit offset, 2 bit length field	
		return writeRegister( configRegister.compReg2, COMP_REG_2);
	}
	return 0;
}

bool ad5560::setCf( uint8_t cf)
{
	if( cf < 8)
	{
		configRegister.compReg2 = bitEditor( 4, 3, configRegister.compReg2, cf); //4 bit offset, 3 bit length field	
		return writeRegister( configRegister.compReg2, COMP_REG_2);
	}
	return 0;
}

bool ad5560::setCc3( bool state)
{
	if( state)
	{
		configRegister.compReg2 |= 0b0000000000001000;
		return writeRegister( configRegister.compReg2, COMP_REG_2);
	}	
	configRegister.compReg2 &= 0b1111111111110111;	
	return writeRegister( configRegister.compReg2, COMP_REG_2);
}
bool ad5560::setCc2( bool state)
{
	if( state)
	{
		configRegister.compReg2 |= 0b0000000000000100;
		return writeRegister( configRegister.compReg2, COMP_REG_2);
	}	
	configRegister.compReg2 &= 0b1111111111111011;	
	return writeRegister( configRegister.compReg2, COMP_REG_2);
}
bool ad5560::setCc1( bool state)
{
	if( state)
	{
		configRegister.compReg2 |= 0b0000000000000010;
		return writeRegister( configRegister.compReg2, COMP_REG_2);
	}	
	configRegister.compReg2 &= 0b1111111111111101;	
	return writeRegister( configRegister.compReg2, COMP_REG_2);
}

//---------------------------------------------------------------------ALARM_SET_REG
bool ad5560::setTMPALM( uint8_t mode) // Latch, disable
{
	if( mode < 4)
	{
		configRegister.alarmSetReg = bitEditor( 14, 2, configRegister.alarmSetReg, mode);
		return writeRegister( configRegister.alarmSetReg, ALARM_SET_REG);
	}
	return 0;
}
bool ad5560::setOSALAM( uint8_t mode) // Latch, disable
{
	if( mode < 4)
	{
		configRegister.alarmSetReg = bitEditor( 12, 2, configRegister.alarmSetReg, mode);
		return writeRegister( configRegister.alarmSetReg, ALARM_SET_REG);
	}
	return 0;
}
bool ad5560::setDUTALM( uint8_t mode) // Latch, disable
{
	if( mode < 4)
	{
		configRegister.alarmSetReg = bitEditor( 10, 2, configRegister.alarmSetReg, mode);
		return writeRegister( configRegister.alarmSetReg, ALARM_SET_REG);
	}
	return 0;
}
bool ad5560::setCLALM( uint8_t mode) // Latch, disable
{
	if( mode < 4)
	{
		configRegister.alarmSetReg = bitEditor( 8, 2, configRegister.alarmSetReg, mode);
		return writeRegister( configRegister.alarmSetReg, ALARM_SET_REG);
	}
	return 0;
}
bool ad5560::setGRDALM( uint8_t mode) // Latch, disable
{
	if( mode < 4)
	{
		configRegister.alarmSetReg = bitEditor( 6, 2, configRegister.alarmSetReg, mode);
		return writeRegister( configRegister.alarmSetReg, ALARM_SET_REG);
	}
	return 0;
}

//------------------------------------------------------------------------clear alarm bits
uint16_t ad5560::clearAlarmBits()
{
	uint16_t dummyReg = 0;
	readRegister( dummyReg, CLEAR_ALARM_BIT_REG);
	return dummyReg;
}

//----------------------------------------------------------------------------------DIAG_REG
bool ad5560::setDIAG( uint8_t mode)
{
	if( mode < 16)
	{
		configRegister.diagnosticReg = bitEditor( 12, 4, configRegister.diagnosticReg, mode);
		return writeRegister( configRegister.diagnosticReg, DIAG_REG);
	}
	return 0;
}

bool ad5560::setTSENSE( uint8_t mode)
{
	if( mode < 32)
	{
		configRegister.diagnosticReg = bitEditor( 7, 5, configRegister.diagnosticReg, mode);
		return writeRegister( configRegister.diagnosticReg, DIAG_REG);
	}
	return 0;
}

bool ad5560::setTestForceAMP( uint8_t mode)
{
	if( mode < 4)
	{
		configRegister.diagnosticReg = bitEditor( 5, 2, configRegister.diagnosticReg, mode);
		return writeRegister( configRegister.diagnosticReg, DIAG_REG);
	}
	return 0;
}

//------------------------------------------------------------------------------------set output

bool ad5560::setOutputVoltage( double outVolt)
{
	if( outVolt <= ( positiveRailVoltage - VOLTAGE_HEADROOM) && outVolt >= ( negativeRailVoltage + VOLTAGE_HEADROOM))
	{
		outputVoltage = outVolt;
		finDac.x1Dac = voltageToCode( outVolt, offsetDacCode);		
		return writeRegister( finDac.x1Dac, FIN_DAC_REG);
	}
	return false;
}

double ad5560::getOutputVoltage()
{
	return outputVoltage;
}

bool ad5560::setOutputVoltageCode( uint16_t x1_dac, uint16_t m_dac, uint16_t c_dac)
{
	bool done = 1;
	finDac.x1Dac = x1_dac;
	done &= writeRegister( x1_dac, FIN_DAC_REG);
	finDac.mDac = m_dac;
	done &= writeRegister( m_dac, FIN_DAC_M_REG);
	finDac.cDac = c_dac;
	done &= writeRegister( c_dac, FIN_DAC_C_REG);
	return done;
}

uint16_t ad5560::getOutputVoltageX1Code()
{
	return finDac.x1Dac;
}

uint16_t ad5560::getOutputVoltageMCode()
{
	return finDac.mDac;
}

uint16_t ad5560::getOutputVoltageCCode()
{
	return finDac.cDac;
}

bool ad5560::finDownloadAndCheck()
{
	bool done = 1;
	uint16_t bufferReg = 0;
	//*******
	done &= readRegister( bufferReg, FIN_DAC_REG);
	if( bufferReg != finDac.x1Dac)
	{
		done &= 0;
		writeRegister( finDac.x1Dac, FIN_DAC_REG);
	}
	//*******
	done &= readRegister( bufferReg, FIN_DAC_M_REG);
	if( bufferReg != finDac.mDac)
	{
		done &= 0;
		writeRegister( finDac.mDac, FIN_DAC_M_REG);
	}
	//*******
	done &= readRegister( bufferReg, FIN_DAC_C_REG);
	if( bufferReg != finDac.cDac)
	{
		done &= 0;
		writeRegister( finDac.cDac, FIN_DAC_C_REG);
	}
	return done;
}

//-----------------------------------------------------------------------------set offset dacCellCompleteSetup

bool ad5560::setOffsetCode( uint16_t code)
{
	offsetDacCode = code;
	return writeRegister( code, OFFSET_DAC_REG);
}

uint16_t ad5560::getOffsetCode()
{
	return offsetDacCode;
}

bool ad5560::setOsdCode( uint16_t code)
{
	osdDacCode = code;
	return writeRegister( code, OSD_REG);
}

bool ad5560::setDgsCode( uint16_t code)
{
	dgsDacCode = code;
	return writeRegister( code, DGS_REG);
}
//-----------------------------------------------------------------------------set comparators
bool ad5560::setCurrentComparator(  uint16_t x1_dac, uint16_t m_dac, uint16_t c_dac, uint16_t address)
{
	if( address < 0x13 || address > 0x3A)
		return 0;
	bool done = 1;
	uint8_t arrayIndex = (address - 0x13) / 3;
	currentComparatorImage[ arrayIndex].x1Dac = x1_dac;
	done &= writeRegister( x1_dac, address);
	currentComparatorImage[ arrayIndex].mDac = m_dac;
	done &= writeRegister( m_dac, address);
	currentComparatorImage[ arrayIndex].cDac = c_dac;
	done &= writeRegister( c_dac, address);
	return done;
}

bool ad5560::setVoltageComparator(  uint16_t x1_dac, uint16_t m_dac, uint16_t c_dac, uint16_t address)
{
	if( address < 0x45)
		return 0;
	bool done = 1;
	uint8_t arrayIndex = (address - 0x45) / 3;
	currentComparatorImage[ arrayIndex].x1Dac = x1_dac;
	done &= writeRegister( x1_dac, address);
	currentComparatorImage[ arrayIndex].mDac = m_dac;
	done &= writeRegister( m_dac, address);
	currentComparatorImage[ arrayIndex].cDac = c_dac;
	done &= writeRegister( c_dac, address);
	return done;
}

//-----------------------------------------------------------------------------set claps
bool ad5560::setClamp(  uint16_t x1_dac, uint16_t m_dac, uint16_t c_dac, uint16_t address)
{
	if( address < 0xD || address > 0x12)
		return 0;
	bool done = 1;
	uint8_t arrayIndex = (address - 0xD) / 3;
	currentComparatorImage[ arrayIndex].x1Dac = x1_dac;
	done &= writeRegister( x1_dac, address);
	currentComparatorImage[ arrayIndex].mDac = m_dac;
	done &= writeRegister( m_dac, address);
	currentComparatorImage[ arrayIndex].cDac = c_dac;
	done &= writeRegister( c_dac, address);
	return done;
}

//----------------------------------------------------------------------------utilities
uint16_t ad5560::voltageToCode( double outputVoltage, uint16_t offsetDacCode)
{
  long outputCode = round( outputVoltage * REF_CONV) + offsetDacCode;
  if( outputCode < 0 || outputCode >= 0xffff)
    return offsetDacCode;
  return ( uint16_t) outputCode;
}

double ad5560::codeToVoltage( uint16_t voltageCode, uint16_t offsetDacCode)
{
  long outputVoltage = (voltageCode-offsetDacCode);
  return outputVoltage / REF_CONV;
}

double ad5560::temperatureSensorReading()
{
  return ((( analogRead(analog_pin) * 0.0048828) - TEMP_OFFSET_VOLTAGE) / TEMP_SENS_COEFFICIENT) + TEMP_SENS_OFFSET_TEMP;
}

void ad5560::isrOverride(void (*function)(void))
{
  __isrOverride = function;
}

//----------------------------------------------------------------------------single ramp
bool ad5560::disableRamp()
{
	return writeRegister( RAMP_DISABLE, RAMP_ENABLE_REG);
}

void ad5560::singleRamp( uint16_t endCode, double riseInterval) //riseInterva in s
{
	if( endCode == finDac.x1Dac)
		return;
  uint16_t rampStep;
	if( finDac.x1Dac < endCode)
		rampStep = endCode - finDac.x1Dac;
	else
		rampStep = finDac.x1Dac - endCode;
	finDac.x1Dac = endCode;
  uint8_t ad5560_stepMult = 1;
  double clockFrequency = rampStep / ( 16 * riseInterval); //16*1^6 , riseInterval in s
  unsigned int counterTarget = rampStep / 16;
  while( clockFrequency > AD5560_MAX_FREQ)
  {
    ad5560_stepMult++;
    clockFrequency = rampStep / ( 16 * ad5560_stepMult * riseInterval);
    counterTarget = rampStep / ( 16 * ad5560_stepMult);
  } 
  
  uint16_t registerContent = endCode;
  writeRegister( registerContent, RAMP_END_CODE_REG);
  registerContent = 0x0000 | ad5560_stepMult;
  writeRegister( registerContent, RAMP_SIZE_REG);
	registerContent = RAMP_RCLK_DIV_DEF0;
	writeRegister( registerContent, RAMP_RCLK_DIV_REG);
	registerContent = RAMP_ENABLE;
  writeRegister( registerContent, RAMP_ENABLE_REG);
	
  OCR5A = (uint16_t) counterTarget + RAMP_CLK_PAD;
  setTimer4Freq( clockFrequency);
  TCCR5B = CLC_EXT_RISE_B_EN;
  TCCR4A = CLC_TIMER_4_RUN;
}

void ad5560::singleRampVoltage( double endVoltage, double riseInterval)
{
  singleRamp( voltageToCode( endVoltage, offsetDacCode), riseInterval);
}

//----------------------------------------------------------------------------multiple run

rampSegment ad5560::computeSingleSegment( uint16_t startCode, uint16_t endCode, double riseInterval)
{
	rampSegment thisRampSegment;
	thisRampSegment.endCode = endCode;
	thisRampSegment.noRamp = 0;
  uint16_t rampStep;
	if( startCode <= endCode)
		rampStep = endCode - startCode;
	else
		rampStep = startCode - endCode;
	uint8_t ad5560_stepMult = 1;
  double clockFrequency = rampStep / ( 16 * riseInterval); //16*1^6 , riseInterval in s
  unsigned int counterTarget = rampStep / 16;
  while( clockFrequency > AD5560_MAX_FREQ)
  {
    ad5560_stepMult++;
    clockFrequency = rampStep / ( 16 * ad5560_stepMult * riseInterval);
    counterTarget = rampStep / ( 16 * ad5560_stepMult);
  }	
	if( rampStep == 0)
	{
		thisRampSegment.noRamp = 0xff;
		counterTarget = RAMP_MAX_COUNTER;
		clockFrequency = RAMP_MAX_COUNTER / riseInterval;
		while( clockFrequency > MAX_FREQ_PRE1) //Se frequenza troppo alta riduco il counter, divido ogni colpo di 8
		{
			counterTarget = counterTarget >> 3;
			clockFrequency = counterTarget / riseInterval;
		}
	}
	thisRampSegment.timerOCR5A = counterTarget + RAMP_CLK_PAD;
	thisRampSegment.stepMult = 0x0000 | ad5560_stepMult;
	thisRampSegment.timerRegisterOCR4A = computeTimer4Register( clockFrequency);
	thisRampSegment.timerPrescalerTCCR4B = computeTimer4Prescaler( clockFrequency);
	return thisRampSegment;
}

void ad5560::startMultipltRamp()
{
	uint16_t registerContent = RAMP_RCLK_DIV_DEF0;
	writeRegister( registerContent, RAMP_RCLK_DIV_REG);
	return;
}


void ad5560::runSingleStep( rampSegment thisRampSegment)
{
	if( thisRampSegment.noRamp == 0)
	{
		finDac.x1Dac = thisRampSegment.endCode;
		writeRegister( thisRampSegment.endCode, RAMP_END_CODE_REG);
		writeRegister( thisRampSegment.stepMult, RAMP_SIZE_REG);
		writeRegister( RAMP_ENABLE, RAMP_ENABLE_REG);
	}
	else
		writeRegister( RAMP_DISABLE, RAMP_ENABLE_REG);
	TCCR4B = thisRampSegment.timerPrescalerTCCR4B;
	OCR4A = thisRampSegment.timerRegisterOCR4A;
	OCR5A = thisRampSegment.timerOCR5A;
  TCCR5B = CLC_EXT_RISE_B_EN;
  TCCR4A = CLC_TIMER_4_RUN;
	return;
}
//******************************************************************************************************************************************
bool ad5560::writeRegisterCore( uint16_t &data_to_write, byte address, bool writeReg)
{
  if(digitalRead( busy_pin) == AD_BUSY)
  {
    return 0;
  }
  //Format the w/r bit
  if( !writeReg)
    address = address | 0b10000000; //set read flag
  else
    address = address & 0b01111111; //set write flag
  //Initialize the buffer array
  uint8_t transferBuffer[3];
  long waitTimeout = micros();
  //set SPI transceiver
  SPI.beginTransaction( SPISettings( spi_clk, MSBFIRST, SPI_MODE1));
  //Chip select
  digitalWrite( sync_pin, SYNC_HIGH);
  //simultaneous send-receive
  transferBuffer[0] = SPI.transfer( address);
  transferBuffer[1] = SPI.transfer( ( uint8_t) ((data_to_write>>8) & 0x00ff));
  transferBuffer[2] = SPI.transfer( ( uint8_t) (data_to_write & 0x00ff));
  //release chip select
  SPI.endTransaction();
  while(digitalRead( busy_pin) == AD_BUSY)
  {
    if( micros() - waitTimeout > SPI_TIMEOUT)
    {
      data_to_write = 0;
      return 0;
    }
  }
  digitalWrite( sync_pin, SYNC_LOW);  
  //trasnfer output if in reading
  if( writeReg == 0)
  {
    data_to_write= (transferBuffer[1]<<8) | transferBuffer[2];
  }
  return 1;
}


void ad5560::setTimer4Freq( double frequency)
{
  if( frequency > MIN_FREQ_PRE1)
  {
    TCCR4B = PRESCALER_1;
    OCR4A = (uint16_t) round( MAX_FREQ_PRE1 / frequency) - 1;
    return;
  }
  if( frequency > MIN_FREQ_PRE8)
  {
    TCCR4B = PRESCALER_8;
    OCR4A = (uint16_t) round( MAX_FREQ_PRE8 / frequency) - 1;
    return;
  }
  if( frequency > MIN_FREQ_PRE64)
  {
    TCCR4B = PRESCALER_64;
    OCR4A = (uint16_t) round( MAX_FREQ_PRE64 / frequency) - 1;
    return;
  }
  if( frequency > MIN_FREQ_PRE256)
  {
    TCCR4B = PRESCALER_256;
    OCR4A = (uint16_t) round( MAX_FREQ_PRE256 / frequency) - 1;
    return;
  }
  if( frequency > MIN_FREQ_PRE1024)
  {
    TCCR4B = PRESCALER_1024;
    OCR4A = (uint16_t) round( MAX_FREQ_PRE1024 / frequency) - 1;
    return;
  }
  return;
}

uint16_t ad5560::computeTimer4Prescaler( double frequency)
{
	if( frequency > MIN_FREQ_PRE1)
  {
    return PRESCALER_1;
  }
  if( frequency > MIN_FREQ_PRE8)
  {
    return PRESCALER_8;
  }
  if( frequency > MIN_FREQ_PRE64)
  {
    return PRESCALER_64;
  }
  if( frequency > MIN_FREQ_PRE256)
  {
    return PRESCALER_256;
  }
  if( frequency > MIN_FREQ_PRE1024)
  {
    return PRESCALER_1024;
  }
  return PRESCALER_1;
}

uint16_t ad5560::computeTimer4Register( double frequency)
{
  if( frequency > MIN_FREQ_PRE1)
  {
    return (uint16_t) round( MAX_FREQ_PRE1 / frequency) - 1;
  }
  if( frequency > MIN_FREQ_PRE8)
  {
    return (uint16_t) round( MAX_FREQ_PRE8 / frequency) - 1;
  }
  if( frequency > MIN_FREQ_PRE64)
  {
    return (uint16_t) round( MAX_FREQ_PRE64 / frequency) - 1;
  }
  if( frequency > MIN_FREQ_PRE256)
  {
    return (uint16_t) round( MAX_FREQ_PRE256 / frequency) - 1;
  }
  if( frequency > MIN_FREQ_PRE1024)
  {
    return (uint16_t) round( MAX_FREQ_PRE1024 / frequency) - 1;
  }
  return 0x0001;
}

ISR(TIMER5_COMPA_vect)
{
  cli();
  TCCR4A = CLC_TIMER_4_STOP;
  TCCR5B = CLC_EXT_RISE_B_DIS;
	TCNT4 = 0x0000;
	TCNT5 = 0x0000;
  __isrOverride();
  sei();
}

uint16_t ad5560::bitEditor( uint8_t bitIndex, uint8_t bitLen, uint16_t inputRegister, uint16_t setValue)
{
	uint16_t bufferReg = 0x0000;
	uint8_t bitLenCounter = bitLen;
	while( bitLenCounter > 0) //place some 1 ( bitLen) starting from bitIndex
	{
		bufferReg |= ( 1 << ( bitLenCounter+bitIndex-1));
		bitLenCounter--;
	}		
	if( bitIndex > 0) //shift the input value
		setValue = (setValue << bitIndex);
	bufferReg = ~bufferReg;	//invert bits
	inputRegister &= bufferReg; //clear the selected bits
	inputRegister |= setValue; //write bits
	return inputRegister;
}

uint16_t ad5560::bitDecoder( uint8_t bitIndex, uint8_t bitLen, uint16_t inputRegister)
{
  uint16_t bufferReg = 0x0000;
  inputRegister = inputRegister >> bitIndex;
  while( bitLen > 0)
  {
    bufferReg |= ( 1 << ( bitLen-1));
    bitLen--;
  }   
  return inputRegister & bufferReg;
}
//**********************************************************************************************************


#ifndef ad5560_h
#define ad5560_h
#include "Arduino.h"
#include "SPI.h"

//timers
#define RCLK 6
#define CLK_COUNT_PIN 47

#define PRESCALER_1     0b00001001
#define PRESCALER_8     0b00001010
#define PRESCALER_64    0b00001011
#define PRESCALER_256   0b00001100
#define PRESCALER_1024  0b00001101


#define MAX_FREQ_PRE1     8000000
#define MAX_FREQ_PRE8     1000000
#define MAX_FREQ_PRE64    125000
#define MAX_FREQ_PRE256   31250
#define MAX_FREQ_PRE1024  7812.5

#define MIN_FREQ_PRE1     122.07
#define MIN_FREQ_PRE8     15.26
#define MIN_FREQ_PRE64    1.91
#define MIN_FREQ_PRE256   0.48
#define MIN_FREQ_PRE1024  0.119

#define CLC_TIMER_4_RUN 0b01000000
#define CLC_TIMER_4_STOP 0b00000000
#define CLC_EXT_RISE_B_EN  0b00001111
#define CLC_EXT_RISE_B_DIS  0b00001000

//voltage reference
#define OFFSET_DAC_CODE 0x8000
#define FIN_DAC_OFFSET 0.025
#define REF_AMP_GAIN 5.125
#define REF_VOLTAGE 5
#define REF_CONV 2557.5
#define MAX_SLEW_RATE 774.7 //millivolt su micros
#define AD5560_MAX_FREQ   833000
#define VOLTAGE_HEADROOM 2.75
#define POSITIVE_RAIL_DEF0 15
#define NEGATIVE_RAIL_DEF0 -15

//register address
#define NOP_REG 0x00
#define SYS_CRTL_REG 0x01
#define DPS_REG_1 0x02
#define DPS_REG_2 0x03
#define COMP_REG_1 0x04
#define COMP_REG_2 0x05
#define ALARM_SET_REG 0x06
#define DIAG_REG 0x07
#define FIN_DAC_REG 0x08
#define FIN_DAC_M_REG 0x09
#define FIN_DAC_C_REG 0x0A
#define OFFSET_DAC_REG 0x0B
#define OSD_REG 0x0C
#define CLL_REG 0x0D //low clamp x1
#define CLH_REG 0x10 //high clamp x1
//Current comparator DAC address
#define CPL_REG_5U 0x13
#define CPL_REG_25U 0x16
#define CPL_REG_250U 0x19
#define CPL_REG_2500U 0x1C
#define CPL_REG_25M 0x1F
#define CPL_REG_EXT2 0x22
#define CPL_REG_EXT1 0x25
#define CPH_REG_5U 0x28
#define CPH_REG_25U 0x2B
#define CPH_REG_250U 0x2E
#define CPH_REG_2500U 0x31
#define CPH_REG_25M 0x34
#define CPH_REG_EXT2 0x37
#define CPH_REG_EXT1 0x39
//Alarm Reg
#define CLEAR_ALARM_BIT_REG 0x44
//Voltage Comparator Dac address
#define CPL_REG_VOLT 0x45
#define CPH_REG_VOLT 0x48
#define DGS_REG 0x3D
//Rampregister
#define RAMP_END_CODE_REG 0x3E
#define RAMP_SIZE_REG 0x3F
#define RAMP_RCLK_DIV_REG 0x40
#define RAMP_ENABLE_REG 0x41
#define RAMP_INTERRUPT_REG 0x42

#define RAMP_CLK_PAD 8
#define RAMP_MAX_COUNTER 0xffff

//register value
#define SYS_CTRL_REG_DEF0 0b0010001000000000
#define DPS_REG_1_DEF0    0b1011000110000000
#define DPS_REG_2_DEF0    0b0000000000000000
#define COMP_REG_1_DEF0   0x0000
#define COMP_REG_2_DEF0   0b0000000100010000
#define RAMP_SIZE_DEF0    0x0001
#define RAMP_RCLK_DIV_DEF0     0x0001
#define RAMP_DISABLE      0x0000
#define RAMP_ENABLE       0xffff

#define DEFAULT_C_DAC			0x8000
#define DEFAULT_M_DAC			0xffff

#define OSD_DAC_DEFAULT 0x1fff
#define DGS_DAC_DEFAULT 0x3333

// timing
#define SPI_TIMEOUT 30000 //micros
#define SYNC_LOW_TIME 1 //micros
#define SYNC_HIGH 0 //se il segno della logica è sbagliato basta invertire
#define SYNC_LOW 1
#define AD_BUSY 0
#define AD_NOT_BUSY 1

//temperature sensor
#define TEMP_SENS_COEFFICIENT 0.0047
#define TEMP_OFFSET_VOLTAGE 1.54
#define TEMP_SENS_OFFSET_TEMP 25

static void (*__isrOverride)(void);
struct registerImage
{
	uint16_t sysCtrlReg = 0;
	uint16_t dpsReg1 = 0;
	uint16_t dpsReg2 = 0;
	uint16_t compReg1 = 0;
	uint16_t compReg2 = 0;
	uint16_t alarmSetReg = 0;
	uint16_t diagnosticReg = 0;
};

typedef struct 
{
	uint16_t x1Dac;
	uint16_t mDac;
	uint16_t cDac;
}DAC;

typedef struct
{
	uint16_t endCode;
	uint16_t stepMult;
	uint16_t timerRegisterOCR4A;
	uint16_t timerPrescalerTCCR4B;
	uint16_t timerOCR5A;
	uint8_t noRamp;
}rampSegment;

class ad5560
{
  public:
    //direct manipulation
    bool writeRegister( uint16_t data_to_write, uint8_t address); //public function
    bool readRegister( uint16_t &data_from_slave, uint8_t address);
    void dacCellCompleteSetup( uint16_t firstDacAddress, uint16_t x1_dac, uint16_t m_dac, uint16_t c_dac);
		bool registerDownloadAndCheck();
    //turn on
    bool startUp( uint16_t busyPin, uint16_t syncPin, uint16_t analogPin, uint32_t spiClk);
		void setSupplyRail( double positiveRail, double negativeRail);
		bool setOffsetAutomatic();
		void setVRef( double V_Ref);
		double getVRef();
		//SysCtrlReg
		bool setShutdownTemp( uint8_t temperature);
		bool setMeasureOutputGain( uint8_t gain);
		uint8_t getMeasureOutputGain();
		bool setFinGnd( bool state);
		bool getFinGnd();
		bool setCpo( bool state);
		bool getCpo();
		bool setPd( bool state);
		bool getPd();
		bool setLoadMode( uint8_t mode);
		uint8_t getLoadMode();
		//DpsReg1
		bool forceEnable( bool enable);
		bool forceState();
		bool setOutputCurrentRange( uint8_t currentRange);
		uint8_t getOutputCurrent();
		bool setComparatorMode( uint8_t mode);
		uint8_t getComparatorMode();
		bool enableMeasureOutput( bool enable);
		bool setMeasureOutput( uint8_t mode);
		uint8_t getMeasureOutput();
		bool enableClamp( bool enable);
		//DpsReg2
		bool setSlewRate( uint8_t sr);
		uint8_t getSlewRate();
		bool setGpo( bool state);
		bool setSlaveMode( uint8_t mode);
		uint8_t getSlaveMode();
		bool setSenseForce( uint8_t mode);
		bool setInt_10k( bool state);
		//CompReg1
		bool setC_Dut( uint8_t cDut);
		bool setC_DutEsr( uint8_t esr);
		bool setSafeMode( bool state);
		//CompReg2
		bool setManualCompensation( bool state);
		bool setRz( uint8_t rz);
		bool setRp( uint8_t rp);
		bool setGm( uint8_t gm);
		bool setCf( uint8_t cf);
		bool setCc3( bool state);
		bool setCc2( bool state);
		bool setCc1( bool state);
		//AlarmSetUp
		bool setTMPALM( uint8_t mode); // Latch, disable
		bool setOSALAM( uint8_t mode); // Latch, disable
		bool setDUTALM( uint8_t mode); // Latch, disable
		bool setCLALM( uint8_t mode); // Latch, disable
		bool setGRDALM( uint8_t mode); // Latch, disable
		//alarm clear
		uint16_t clearAlarmBits();
		//Diagnostic Reg
		bool setDIAG( uint8_t mode);
		bool setTSENSE( uint8_t mode);
		bool setTestForceAMP( uint8_t mode);
		//set output 
		bool setOutputVoltage( double outVolt);
		double getOutputVoltage();
		bool setOutputVoltageCode( uint16_t x1_dac, uint16_t m_dac, uint16_t c_dac);
		uint16_t getOutputVoltageX1Code();
		uint16_t getOutputVoltageMCode();
		uint16_t getOutputVoltageCCode();
		bool finDownloadAndCheck();
		//set offset dac
		bool setOffsetCode( uint16_t code);
		uint16_t getOffsetCode();
		bool setOsdCode( uint16_t code);
		bool setDgsCode( uint16_t code);
		//set compartors
		bool setCurrentComparator( uint16_t x1_dac, uint16_t m_dac, uint16_t c_dac, uint16_t address);
		bool setVoltageComparator( uint16_t x1_dac, uint16_t m_dac, uint16_t c_dac, uint16_t address);
		//set Clamps
		bool setClamp( uint16_t x1_dac, uint16_t m_dac, uint16_t c_dac, uint16_t address);
    //utilities
    uint16_t voltageToCode( double outputVoltage, uint16_t offsetDacCode);
    double codeToVoltage( uint16_t voltageCode, uint16_t offsetDacCode);
    double temperatureSensorReading();
    void isrOverride(void (*function)(void));
		//single ramp
		bool disableRamp();
		void singleRamp( uint16_t endCode, double riseInterval);
		void singleRampVoltage( double endVoltage, double riseInterval);
		//multiple ramp;
		rampSegment computeSingleSegment( uint16_t startCode, uint16_t endCode, double riseInterval);
		void runSingleStep( rampSegment thisRampSegment);
		void startMultipltRamp();
  private:
    bool writeRegisterCore( uint16_t &data_to_write, byte address, bool writeReg);
    void setTimer4Freq( double frequency);
		uint16_t computeTimer4Prescaler( double frequency);
		uint16_t computeTimer4Register( double frequency);
		uint16_t bitEditor( uint8_t bitIndex, uint8_t bitLen, uint16_t inputRegister, uint16_t setValue);
		uint16_t bitDecoder( uint8_t bitIndex, uint8_t bitLen, uint16_t inputRegister);
		//-----------
		double vRef = REF_VOLTAGE;
    uint32_t spi_clk;
    uint16_t busy_pin, sync_pin, analog_pin;
		uint16_t offsetDacCode = OFFSET_DAC_CODE, osdDacCode = OSD_DAC_DEFAULT, dgsDacCode = DGS_DAC_DEFAULT;
		double outputVoltage, positiveRailVoltage = POSITIVE_RAIL_DEF0, negativeRailVoltage = NEGATIVE_RAIL_DEF0;
		struct registerImage configRegister;
		DAC finDac;
		DAC currentComparatorImage[14];
		DAC voltageComparatorImage[2];
		DAC clampImage[2];
};

#endif